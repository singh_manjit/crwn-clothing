//practice component
import React from 'react';
import './App.css';
import HomePage from './pages/hompage/homepage.component'
import {Route,Switch,Link} from 'react-router-dom'

const test = (props)=>{
  console.log(props)
  const p = [0,1,2,3,4,5,6,7,8];
  return (
  <div>
    {p.map((k)=>(
    <button key={k} onClick={()=>props.history.push(`/test/${k}`)}>test {k}</button>

    ))}

 
  </div>
  )
  }

  const testDetails = (props) =>{
    console.log(props)
    return (
      <div>
        <button onClick={()=>props.history.push("/test")}>test</button>
        test details {props.match.params.id}
        <br></br>
        <Link to="/" >home</Link>
      </div>
    )
  }

function App(props) {
  console.log(props)
  return (
    <div className="App">
      <Switch>
      <Route exact path="/" component={HomePage}/>
      <Route  exact path="/test" component={test} />
      <Route exact path="/test/:id" component={testDetails}/>
      </Switch> 
    </div>
  );
}

export default App;
