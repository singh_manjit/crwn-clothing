import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDD09CUPt_HmmV1qqIsNvZAUGxjQOPSq9Y",
    authDomain: "crwn-db-69acf.firebaseapp.com",
    databaseURL: "https://crwn-db-69acf.firebaseio.com",
    projectId: "crwn-db-69acf",
    storageBucket: "",
    messagingSenderId: "1002004347227",
    appId: "1:1002004347227:web:095dd1249fbfa96a10027e",
    measurementId: "G-8MVXX9VD46"
}

firebase.initializeApp(config); 

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt : 'select_account'});

export const signInWithGoogle = () => auth.signInWithPopup(provider);
export default firebase;