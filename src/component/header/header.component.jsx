import React from 'react'
import { Link } from 'react-router-dom'

import './header.style.scss'
import logo from '../../assets/crown.svg'

const Header = ({ currentUser }) => (
<div className="header">
    <Link to="/" className="logo-container">
        {/* <Logo className="logo"/> */}
        <img src={logo} className="logo" alt="logo" />
    </Link>
    <div className="options">
        <Link className="option" to="/shop" >
            Shop
        </Link>
        <Link className="option" to="/">
            Contact
        </Link>
        {  
            currentUser
            ?
            <div className="options">
                Sign Out
            </div> : 
            <Link className="options" to="/signin">
                Sign In
            </Link>  
        }

    </div>

</div>
)

export default Header;