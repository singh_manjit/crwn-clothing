import React from 'react'

import './sign-in.style.scss'
import FormInput from '../form-input/form-input.component'
import CustomButton from '../custom-button/custom-buttom.component'
import { signInWithGoogle } from '../firebase/firebase.util'

class SignIn extends React.Component{
    constructor(){
        super()
        this.state = {
            email : '',
            password : ''
        }
    }

    handleSubmit(event){
        event.preventDefault()
        console.log("submit")
    }

    onChange(event){
        const { value,name } = event.target;
        
        this.setState({[name]:value});
        console.log(this.state)
    }

    render(){
        return <div className="sign-in">
            <h2>Aleready have An Account</h2>
            <span>sign in woith crendential</span>

            <form onSubmit={this.handleSubmit}>
                <FormInput 
                type="text"  
                name="email" 
                value={this.state.email} 
                handleChange={this.handleSubmit}
                label="Email"
                />
                <FormInput 
                type="text" 
                name="password"
                value={this.state.password} 
                handleChange={this.handleSubmit}
                label="Password"
                />
                <div className="buttons">

                <CustomButton type="submit" >
                    Sign In
                </CustomButton>
                <CustomButton onClick={signInWithGoogle}
                    isGoogleSignIn >
                    With Google
                </CustomButton>
                
                </div>
            </form>
        </div>

    }
}

export default SignIn;