import React from 'react';

import SHOP_DATA from './shop_data'
import CollectionPreview from '../../component/collection-preview/collection-preview.components';

class Shop extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            collections:SHOP_DATA
        }
    }

    render(){
       const  { collections }=this.state
       console.log(this.state)
        return (
            <div>
                {
                collections.map(({id,...otherItems}) =>(
                    <CollectionPreview key={id}  {...otherItems}  />  
                    ))  
                }
            </div>)
    } 
}

export default Shop;