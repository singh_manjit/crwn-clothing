import React from 'react';
import './App.css';
import {Route,Switch} from 'react-router-dom'

import HomePage from './pages/hompage/homepage.component'
import Shop from './pages/shop/shop.component';
import Header from './component/header/header.component'
import SignInSignup from './pages/sign-in-sign-up/sign-in-signup.component';
import { auth } from './component/firebase/firebase.util';




class App extends React.Component{

  constructor(){
    super()
    this.state = {
      user : null
    }
  }
  unsubscribe=null;
  componentDidMount(){
    console.log(this.state.user)
    this.unsubscribe = auth.onAuthStateChanged((user)=>{
      this.setState({user:user})
      console.log(this.state.user)
    })
  

  }

  componentWillUnmount(){
    this.unsubscribe()
  }



  render(){
    console.log(this,this.state.user)
    return (
      <div className="App">
        <Header currentUser={this.state.user}/>
        <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route exact path="/signin" component={SignInSignup}/>
        <Route exact path="/shop" component={Shop}/>
        </Switch> 
      </div>
    );
  }
}

export default App;
